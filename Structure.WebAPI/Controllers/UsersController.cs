using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.Common.DTO.User;

namespace Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpGet]
        public async Task<IEnumerable<UserDto>> GetUsersAsync()
        {
            return await _userService.GetUsersAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserDto>> GetUserAsync(int id)
        {
            var user = await _userService.GetUserAsync(id);
            
            if (user == null) return NotFound($"User with id {id} does not exist");
            return user;
        }

        [HttpPost]
        public async Task<ActionResult<NewUserDto>> CreateUserAsync([FromBody] NewUserDto userDto)
        {
            try
            {
                await _userService.AddUserAsync(userDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
            return Created(new Uri($"{Request.Path}", UriKind.Relative), userDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<UserDto>> UpdateUserAsync(int id, [FromBody] UserDto userDto)
        {
            if (await _userService.GetUserAsync(id) == null) return NotFound($"User with id {id} does not exist");

            userDto.Id = id;
            
            try
            {
                await _userService.UpdateUserAsync(userDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return userDto;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteUserAsync(int id)
        {
            if (await _userService.GetUserAsync(id) == null) return NotFound($"User with id {id} does not exist");

            try
            {
                await _userService.RemoveUserAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}