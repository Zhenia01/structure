using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.Common.DTO.Team;

namespace Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TeamsController : ControllerBase
    {
        private readonly ITeamService _teamService;

        public TeamsController(ITeamService teamService)
        {
            _teamService = teamService;
        }

        [HttpGet]
        public async Task<IEnumerable<TeamDto>> GetTeamsAsync()
        {
            return await _teamService.GetTeamsAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TeamDto>> GetTeamAsync(int id)
        {
            var team = await _teamService.GetTeamAsync(id);
            if (team == null) return NotFound($"Team with id {id} does not exist");
            return team;
        }

        [HttpPost]
        public async Task<ActionResult<TeamDto>> CreateTeamAsync([FromBody] TeamDto teamDto)
        {
            try
            {
                await _teamService.AddTeamAsync(teamDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }
            
            return Created(new Uri($"{Request.Path}", UriKind.Relative), teamDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TeamDto>> UpdateTeamAsync(int id, [FromBody] TeamDto teamDto)
        {
            if (await _teamService.GetTeamAsync(id) == null) return NotFound($"Team with id {id} does not exist");

            teamDto.Id = id;

            try
            {
                await _teamService.UpdateTeamAsync(teamDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return teamDto;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTeamAsync(int id)
        {
            if (await _teamService.GetTeamAsync(id) == null) return NotFound($"Team with id {id} does not exist");

            try
            {
                await _teamService.RemoveTeamAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}