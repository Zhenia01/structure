using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Services;
using Structure.Common.DTO.Task;
using Structure.Common.LinqQueriesModels;

namespace Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LinqController : ControllerBase
    {
        private readonly LinqService _linqService;

        public LinqController(LinqService linqService)
        {
            _linqService = linqService;
        }

        [HttpGet("TasksWithNamesUpTo45/{id}")]
        public async Task<IEnumerable<TaskDto>> TasksWithNamesUpTo45Async(int id)
        {
            return await _linqService.TasksWithNamesUpTo45Async(id);
        }
        
        [HttpGet("UserReport/{id}")]
        public async Task<UserReport> UserReportAsync(int id)
        {
            return await _linqService.UserReportAsync(id);
        }
        
        [HttpGet("ProjectReports")]
        public async Task<IEnumerable<ProjectReport>> ProjectReportsAsync()
        {
            return await _linqService.ProjectReportsAsync();
        }
        
        [HttpGet("TasksFinishedThisYear/{id}")]
        public async Task<IEnumerable<TaskInfo>> TasksFinishedThisYearAsync(int id)
        {
            return await _linqService.TasksFinishedThisYearAsync(id);
        }

        [HttpGet("SortedTeamsWithAllMembersOlderThan10")]
        public async Task<IEnumerable<TeamInfo>> SortedTeamsWithAllMembersOlderThan10Async()
        {
            return await _linqService.SortedTeamsWithAllMembersOlderThan10Async();
        }
        
        [HttpGet("SortedUsersAndTasks")]
        public async Task<IEnumerable<UserAndTasks>> SortedUsersAndTasksAsync()
        {
            return await _linqService.SortedUsersAndTasksAsync();
        }
        
        [HttpGet("TasksPerProjectCount/{id}")]
        public async Task<IEnumerable<ProjectTasks>> TasksPerProjectCountAsync(int id)
        {
            return await _linqService.TasksPerProjectCountJsonHelperAsync(id);
        }
    }
}