using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.Common.DTO.Project;

namespace Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ProjectsController : ControllerBase
    {
        private readonly IProjectService _projectService;

        public ProjectsController(IProjectService projectService)
        {
            _projectService = projectService;
        }

        [HttpGet]
        public async Task<IEnumerable<ProjectDto>> GetProjects()
        {
            return await _projectService.GetProjectsAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDto>> GetProject(int id)
        {
            var project = await _projectService.GetProjectAsync(id);
            if (project == null) return NotFound($"Project with id {id} does not exist");
            return project;
        }

        [HttpPost]
        public async Task<ActionResult<ProjectDto>> CreateProject([FromBody] NewProjectDto projectDto)
        {
            try
            {
                await _projectService.AddProjectAsync(projectDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created(new Uri($"{Request.Path}", UriKind.Relative), projectDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<ProjectDto>> UpdateProject(int id, [FromBody] ProjectDto projectDto)
        {
            if (await _projectService.GetProjectAsync(id) == null) return NotFound($"Project with id {id} does not exist");

            projectDto.Id = id;

            try
            {
                await _projectService.UpdateProjectAsync(projectDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return projectDto;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProjectAsync(int id)
        {
            if (await _projectService.GetProjectAsync(id) == null) return NotFound($"Project with id {id} does not exist");

            try
            {
                await _projectService.RemoveProjectAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }
    }
}