using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.Common.DTO.Task;

namespace Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TasksController : ControllerBase
    {
        private readonly ITaskService _taskService;

        public TasksController(ITaskService taskService)
        {
            _taskService = taskService;
        }

        [HttpGet]
        public async Task<IEnumerable<TaskDto>> GetTasksAsync()
        {
            return await _taskService.GetTasksAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskDto>> GetTaskAsync(int id)
        {
            var task = await _taskService.GetTaskAsync(id);
            if (task == null) return NotFound($"Task with id {id} does not exist");
            return task;
        }

        [HttpPost]
        public async Task<ActionResult<TaskDto>> CreateTaskAsync([FromBody] NewTaskDto taskDto)
        {
            try
            {
                await _taskService.AddTaskAsync(taskDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created(new Uri($"{Request.Path}", UriKind.Relative), taskDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TaskDto>> UpdateTaskAsync(int id, [FromBody] TaskDto taskDto)
        {
            if (await _taskService.GetTaskAsync(id) == null) return NotFound($"Task with id {id} does not exist");

            taskDto.Id = id;
            try
            {
                await _taskService.UpdateTaskAsync(taskDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return taskDto;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTaskAsync(int id)
        {
            if (await _taskService.GetTaskAsync(id) == null) return NotFound($"Task with id {id} does not exist");
            try
            {
                await _taskService.RemoveTaskAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return NoContent();
        }

        [HttpPatch("mark/{id}")]
        public async Task<ActionResult<int>> MarkTaskAsFinished(int id, [FromServices] ITaskStateService service)
        {
            if (await _taskService.GetTaskAsync(id) == null) return NotFound($"Task with id {id} does not exist");
            await _taskService.MarkTaskAsFinishedAsync(id, service);
            return id;
        }
    }
}