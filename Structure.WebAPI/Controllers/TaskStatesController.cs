using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.Common.DTO.TaskState;

namespace Structure.WebAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class TaskStatesController : ControllerBase
    {
        private readonly ITaskStateService _taskStateService;

        public TaskStatesController(ITaskStateService taskStateService)
        {
            _taskStateService = taskStateService;
        }

        [HttpGet]
        public async Task<IEnumerable<TaskStateDto>> GetTaskStates()
        {
            return await _taskStateService.GetTaskStatesAsync();
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<TaskStateDto>> GetTaskState(int id)
        {
            var taskState = await _taskStateService.GetTaskStateAsync(id);
            if (taskState == null) return NotFound($"TaskState with id {id} does not exist");
            return taskState;
        }

        [HttpPost]
        public async Task<ActionResult<TaskStateDto>> CreateTaskState([FromBody] TaskStateDto taskStateDto)
        {
            try
            {
                await _taskStateService.AddTaskStateAsync(taskStateDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }

            return Created(new Uri($"{Request.Path}", UriKind.Relative), taskStateDto);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult<TaskStateDto>> UpdateTaskState(int id, [FromBody] TaskStateDto taskStateDto)
        {
            if (await _taskStateService.GetTaskStatesAsync() == null) return NotFound($"TaskState with id {id} does not exist");

            taskStateDto.Id = id;
            try
            {
                await _taskStateService.UpdateTaskStateAsync(taskStateDto);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }


            return taskStateDto;
        }

        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTaskState(int id)
        {
            if (await _taskStateService.GetTaskStatesAsync() == null) return NotFound($"TaskState with id {id} does not exist");

            try
            {
                await _taskStateService.RemoveTaskStateAsync(id);
            }
            catch (ArgumentException e)
            {
                return BadRequest(e.Message);
            }


            return NoContent();
        }
    }
}