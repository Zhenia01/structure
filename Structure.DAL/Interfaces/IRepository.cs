using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Structure.DAL.Entities.Common;

namespace Structure.DAL.Interfaces
{
    public interface IRepository<T> where T : Entity
    {
        Task<T> GetByIdAsync(int id);

        Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> filter = null);

        Task AddAsync(T entity);

        Task RemoveAsync(T entity);
        
        Task RemoveAsync(int id);

        Task<T> UpdateAsync(T entity);
    }
}