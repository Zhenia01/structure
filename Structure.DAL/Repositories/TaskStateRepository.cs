using Structure.DAL.Entities;

namespace Structure.DAL.Repositories
{
    public class TaskStateRepository : Repository<TaskState>
    {
        public TaskStateRepository(Context.StorageContext storageContext) : base(storageContext)
        {
        }
    }
}