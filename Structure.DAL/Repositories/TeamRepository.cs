using Structure.DAL.Entities;

namespace Structure.DAL.Repositories
{
    public class TeamRepository : Repository<Team>
    {
        public TeamRepository(Context.StorageContext storageContext) : base(storageContext)
        {
        }
    }
}