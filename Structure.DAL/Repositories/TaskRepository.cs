using Structure.DAL.Entities;

namespace Structure.DAL.Repositories
{
    public class TaskRepository : Repository<Task>
    {
        public TaskRepository(Context.StorageContext storageContext) : base(storageContext)
        {
            
        }
    }
}