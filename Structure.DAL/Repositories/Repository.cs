using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Structure.DAL.Entities.Common;
using Structure.DAL.Interfaces;

namespace Structure.DAL.Repositories
{
    public abstract class Repository<T>
        : IRepository<T> where T : Entity
    {
        private protected readonly Context.StorageContext StorageContext;

        protected Repository(Context.StorageContext storageContext)
        {
            StorageContext = storageContext;
        }

        public virtual async Task<T> GetByIdAsync(int id)
        {
            return await StorageContext.Set<T>().AsNoTracking().FirstOrDefaultAsync(e => e.Id == id);
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync(Expression<Func<T, bool>> filter = null)
        {
            var dbSet = StorageContext.Set<T>().AsNoTracking();

            if (filter != null)
            {
                return await Task.Run(() => dbSet.Where(filter));
            }

            return dbSet;
        }

        public virtual async Task AddAsync(T entity)
        {
            await StorageContext.Set<T>().AddAsync(entity);
            try
            {
                await StorageContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                throw new ArgumentException("Wrong body or parameters", e);
            }
        }

        public virtual async Task RemoveAsync(T entity)
        {
            var dbSet = StorageContext.Set<T>();
            if (StorageContext.Entry(entity).State == EntityState.Detached)
            {
                dbSet.Attach(entity);
            }

            try
            {
                dbSet.Remove(entity);
                await StorageContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                throw new ArgumentException("Wrong body or parameters", e);
            }
        }

        public virtual async Task RemoveAsync(int id)
        {
            T entity = await StorageContext.Set<T>().FindAsync(id);
            await RemoveAsync(entity);
        }

        public virtual async Task<T> UpdateAsync(T entity)
        {
            StorageContext.Set<T>().Attach(entity);
            StorageContext.Entry(entity).State = EntityState.Modified;
            try
            {
                await StorageContext.SaveChangesAsync();
            }
            catch (DbUpdateException e)
            {
                throw new ArgumentException("Wrong body or parameters", e);
            }

            return entity;
        }
    }
}