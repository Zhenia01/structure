using Structure.DAL.Entities;

namespace Structure.DAL.Repositories
{
    public class ProjectRepository : Repository<Project>
    {
        public ProjectRepository(Context.StorageContext storageContext) : base(storageContext)
        {
        }
    }
}