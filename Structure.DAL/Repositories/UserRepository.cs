using Structure.DAL.Entities;

namespace Structure.DAL.Repositories
{
    public class UserRepository : Repository<User>
    {
        public UserRepository(Context.StorageContext storageContext) : base(storageContext)
        {
        }
    }
}