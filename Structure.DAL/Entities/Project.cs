using System;
using System.Collections.Generic;
using Structure.DAL.Entities.Common;

namespace Structure.DAL.Entities
{
    public class Project : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime Deadline { get; set; }

        public int AuthorId { get; set; }
        public User Author { get; set; }
        
        public int TeamId { get; set; }
        public Team Team { get; set; }
        
        public List<Task> Tasks { get; set; }
    }
}