using System;
using System.Collections.Generic;
using Structure.DAL.Entities.Common;

namespace Structure.DAL.Entities
{
    public class User : Entity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime RegisteredAt { get; set; }

        public int? TeamId { get; set; }
        
        public Team Team { get; set; }
        
        public List<Task> Tasks { get; set; }
        
        public List<Project> Projects { get; set; }
    }
}