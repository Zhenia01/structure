using System;
using System.Collections.Generic;
using Structure.DAL.Entities.Common;

namespace Structure.DAL.Entities
{
    public class Team : Entity
    {
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
        
        public List<User> Members { get; set; }
        
        public List<Project> Projects { get; set; }
    }
}