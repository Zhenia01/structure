using System;
using Structure.DAL.Entities.Common;

namespace Structure.DAL.Entities
{
    public class Task : Entity
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        
        public int TaskStateId { get; set; }
        public TaskState TaskState { get; set; }
        
        public int ProjectId { get; set; }
        public Project Project { get; set; }
        
        public int PerformerId { get; set; }
        public User Performer { get; set; }
    }
}