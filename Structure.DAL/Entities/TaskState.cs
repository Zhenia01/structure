using System.Collections.Generic;
using Structure.DAL.Entities.Common;

namespace Structure.DAL.Entities
{
    public class TaskState : Entity
    {
        public string Value { get; set; }
        
        public List<Task> Tasks { get; set; }
    }
}