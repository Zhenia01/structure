using Microsoft.EntityFrameworkCore;
using Structure.DAL.Entities;
using Task = Structure.DAL.Entities.Task;

namespace Structure.DAL.Context
{
    public class StorageContext : DbContext
    {
        public DbSet<Project> Projects { get; private set; }
        public DbSet<Team> Teams { get; private set; }
        public DbSet<Task> Tasks { get; private set; }
        public DbSet<User> Users { get; private set; }
        public DbSet<TaskState> TaskStates { get; private set; }

        public StorageContext(DbContextOptions<StorageContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Configure();

            modelBuilder.Seed();
        }
    }
}