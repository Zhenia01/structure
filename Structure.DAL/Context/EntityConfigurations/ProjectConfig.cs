using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.DAL.Entities;

namespace Structure.DAL.Context.EntityConfigurations
{
    public class ProjectConfig : IEntityTypeConfiguration<Project>
    {
        public void Configure(EntityTypeBuilder<Project> entity)
        {
            entity.Property(p => p.Name).IsRequired();
            entity.Property(p => p.Description).IsRequired();
            entity.HasOne(u => u.Author).WithMany(u => u.Projects).HasForeignKey(p => p.AuthorId).IsRequired().OnDelete(DeleteBehavior.NoAction);
            entity.ToTable("Projects");
        }
    }
}