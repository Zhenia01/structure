using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.DAL.Entities;

namespace Structure.DAL.Context.EntityConfigurations
{
    public class TeamConfig : IEntityTypeConfiguration<Team>
    {
        public void Configure(EntityTypeBuilder<Team> entity)
        {
            entity.Property(t => t.Name).IsRequired().HasMaxLength(25);
            entity.ToTable("Teams");
        }
    }
}