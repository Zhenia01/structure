using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.DAL.Entities;

namespace Structure.DAL.Context.EntityConfigurations
{
    public class TaskConfig : IEntityTypeConfiguration<Task>
    {
        public void Configure(EntityTypeBuilder<Task> entity)
        {
            entity.Property(t => t.Name).IsRequired();
            entity.Property(t => t.Description).IsRequired();
            entity.HasOne(t => t.Performer).WithMany(u => u.Tasks).HasForeignKey(t => t.PerformerId).IsRequired()
                .OnDelete(DeleteBehavior.NoAction);
            entity.ToTable("Tasks");
        }
    }
}