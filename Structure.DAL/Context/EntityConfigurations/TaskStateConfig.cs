using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.DAL.Entities;

namespace Structure.DAL.Context.EntityConfigurations
{
    public class TaskStateConfig : IEntityTypeConfiguration<TaskState>
    {
        public void Configure(EntityTypeBuilder<TaskState> entity)
        {
            entity.Property(ts => ts.Value).IsRequired().HasMaxLength(15); 
            entity.ToTable("TaskStates");
        }
    }
}