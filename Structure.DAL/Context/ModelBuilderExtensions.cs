﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text.Json;
using Microsoft.EntityFrameworkCore;
using Structure.DAL.Context.EntityConfigurations;
using Structure.DAL.Entities;

namespace Structure.DAL.Context
{
    public static class ModelBuilderExtensions
    {
        
        public static void Configure(this ModelBuilder modelBuilder)
        {
            // apply configurations from EntityConfigurations folder
            modelBuilder.ApplyConfigurationsFromAssembly(typeof(ProjectConfig).Assembly);
        }

        private static readonly string DataPath = Path.Combine(Directory.GetParent(Assembly.GetExecutingAssembly().Location).Parent!.Parent!.Parent!.Parent!.FullName, "Structure.DAL", "Data");

        public static void Seed(this ModelBuilder modelBuilder)
        {
            using StreamReader projectsSr = new StreamReader(Path.Combine(DataPath,  "projects.json"));
            var projects = JsonSerializer.Deserialize<IEnumerable<Project>>(projectsSr.ReadToEnd());

            using StreamReader usersSr = new StreamReader(Path.Combine(DataPath,  "users.json"));
            var users = JsonSerializer.Deserialize<IEnumerable<User>>(usersSr.ReadToEnd());
            
            using StreamReader teamsSr = new StreamReader(Path.Combine(DataPath,  "teams.json"));
            var teams = JsonSerializer.Deserialize<IEnumerable<Team>>(teamsSr.ReadToEnd());
            
            using StreamReader tasksSr = new StreamReader(Path.Combine(DataPath, "tasks.json"));
            var tasks = JsonSerializer.Deserialize<IEnumerable<Task>>(tasksSr.ReadToEnd());
            
            using StreamReader taskStatesSr = new StreamReader(Path.Combine(DataPath, "taskStates.json"));
            var taskStates = JsonSerializer.Deserialize<IEnumerable<TaskState>>(taskStatesSr.ReadToEnd());

            modelBuilder.Entity<TaskState>().HasData(taskStates);
            modelBuilder.Entity<Project>().HasData(projects);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<Task>().HasData(tasks);
        }
    }
}
