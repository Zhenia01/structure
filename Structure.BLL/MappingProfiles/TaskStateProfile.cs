using AutoMapper;
using Structure.Common.DTO.TaskState;
using Structure.DAL.Entities;

namespace Structure.BLL.MappingProfiles
{
    public class TaskStateProfile : Profile
    {
        public TaskStateProfile()
        {
            CreateMap<TaskState, TaskStateDto>();
            CreateMap<TaskStateDto, TaskState>();
        }
    }
}