using AutoMapper;
using Structure.Common.DTO.Team;
using Structure.DAL.Entities;

namespace Structure.BLL.MappingProfiles
{
    public class TeamProfile : Profile
    {
        public TeamProfile()
        {
            CreateMap<Team, TeamDto>();
            CreateMap<TeamDto, Team>();
        }
    }
}