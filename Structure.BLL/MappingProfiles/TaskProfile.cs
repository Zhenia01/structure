using AutoMapper;
using Structure.Common.DTO.Task;
using Structure.DAL.Entities;

namespace Structure.BLL.MappingProfiles
{
    public class TaskProfile : Profile
    {
        public TaskProfile()
        {
            CreateMap<Task, TaskDto>();
            CreateMap<TaskDto, Task>();
            CreateMap<NewTaskDto, Task>();
        }
    }
}