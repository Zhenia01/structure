using AutoMapper;
using Structure.Common.DTO.Project;
using Structure.DAL.Entities;

namespace Structure.BLL.MappingProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDto>();
            CreateMap<ProjectDto, Project>();
            CreateMap<NewProjectDto, Project>();
        }
    }
}