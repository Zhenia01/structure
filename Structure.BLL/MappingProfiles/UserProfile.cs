using AutoMapper;
using Structure.Common.DTO.User;
using Structure.DAL.Entities;

namespace Structure.BLL.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserDto>();
            CreateMap<UserDto, User>();
            CreateMap<NewUserDto, User>();
        }
    }
}