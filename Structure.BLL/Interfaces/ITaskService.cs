using System.Collections.Generic;
using System.Threading.Tasks;
using Structure.Common.DTO.Task;

namespace Structure.BLL.Interfaces
{
    public interface ITaskService
    {
        public Task<IEnumerable<TaskDto>> GetTasksAsync();
        public Task<TaskDto> GetTaskAsync(int id);
        public Task AddTaskAsync(NewTaskDto userDto);
        public Task RemoveTaskAsync(int id);
        public Task UpdateTaskAsync(TaskDto userDto);
        public Task MarkTaskAsFinishedAsync(int id, ITaskStateService service);
    }
}