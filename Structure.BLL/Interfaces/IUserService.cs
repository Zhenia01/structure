using System.Collections.Generic;
using System.Threading.Tasks;
using Structure.Common.DTO.User;

namespace Structure.BLL.Interfaces
{
    public interface IUserService
    {
        public Task<IEnumerable<UserDto>> GetUsersAsync();
        public Task<UserDto> GetUserAsync(int id);
        public Task AddUserAsync(NewUserDto userDto);
        public Task RemoveUserAsync(int id);
        public Task UpdateUserAsync(UserDto userDto);
        public Task AddUserToTeamAsync(int userId, int teamId, ITeamService service);
    }
}