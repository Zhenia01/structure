using System.Collections.Generic;
using System.Threading.Tasks;
using Structure.Common.DTO.Team;

namespace Structure.BLL.Interfaces
{
    public interface ITeamService
    {
        public Task<IEnumerable<TeamDto>> GetTeamsAsync();
        public Task<TeamDto> GetTeamAsync(int id);
        public Task AddTeamAsync(TeamDto userDto);
        public Task RemoveTeamAsync(int id);
        public Task UpdateTeamAsync(TeamDto userDto);
    }
}