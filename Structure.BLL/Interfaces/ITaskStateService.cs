using System.Collections.Generic;
using System.Threading.Tasks;
using Structure.Common.DTO.TaskState;

namespace Structure.BLL.Interfaces
{
    public interface ITaskStateService
    {
        public Task<IEnumerable<TaskStateDto>> GetTaskStatesAsync();
        public Task<TaskStateDto> GetTaskStateAsync(int id);
        public Task AddTaskStateAsync(TaskStateDto userDto);
        public Task RemoveTaskStateAsync(int id);
        public Task UpdateTaskStateAsync(TaskStateDto userDto);
        public Task<TaskStateDto> GetFinishedTaskStateAsync();
    }
}