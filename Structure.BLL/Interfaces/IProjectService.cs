using System.Collections.Generic;
using System.Threading.Tasks;
using Structure.Common.DTO.Project;

namespace Structure.BLL.Interfaces
{
    public interface IProjectService
    {
        public Task<IEnumerable<ProjectDto>> GetProjectsAsync();
        public Task<ProjectDto> GetProjectAsync(int id);
        public Task AddProjectAsync(NewProjectDto userDto);
        public Task RemoveProjectAsync(int id);
        public Task UpdateProjectAsync(ProjectDto userDto);
    }
}