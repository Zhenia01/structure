using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Structure.BLL.Services.Abstract;
using Structure.Common.DTO.Project;
using Structure.Common.DTO.Task;
using Structure.Common.DTO.User;
using Structure.Common.LinqQueriesModels;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Task = Structure.DAL.Entities.Task;

namespace Structure.BLL.Services
{
    public class LinqService : ServiceBase
    {
        private readonly IRepository<Project> _projectRep;
        private readonly IRepository<Team> _teamRep;
        private readonly IRepository<Task> _taskRep;
        private readonly IRepository<TaskState> _taskStateRep;
        private readonly IRepository<User> _userRep;

        public LinqService(
            IRepository<Project> projectRep,
            IRepository<Team> teamRep,
            IRepository<Task> taskRep,
            IRepository<TaskState> taskStateRep,
            IRepository<User> userRep,
            IMapper mapper
        ) : base(mapper)
        {
            _projectRep = projectRep;
            _teamRep = teamRep;
            _taskRep = taskRep;
            _taskStateRep = taskStateRep;
            _userRep = userRep;
        }

        public async Task<IEnumerable<ProjectTasks>> TasksPerProjectCountJsonHelperAsync(int id)
        {
            var dict = await TasksPerProjectCountAsync(id);
            return dict.Select(pair => new ProjectTasks
            {
                Project = pair.Key,
                Count = pair.Value
            }).ToList();
        }

        public async Task<IDictionary<ProjectDto, int>> TasksPerProjectCountAsync(int userId)
        {
            var projects = (await _projectRep.GetAllAsync()).ToList();
            var tasks = (await _taskRep.GetAllAsync()).ToList();

            return await System.Threading.Tasks.Task.Run(() =>
                projects
                    .Where(p => p.AuthorId == userId)
                    .GroupJoin(
                        tasks,
                        p => p.Id,
                        t => t.ProjectId,
                        (project, projectTasks) =>
                        {
                            project.Tasks = projectTasks.ToList();
                            return project;
                        })
                    .ToDictionary(p => Mapper.Map<ProjectDto>(p), p => p.Tasks.Count));
        }

        public async Task<IEnumerable<TaskDto>> TasksWithNamesUpTo45Async(int userId)
        {
            return (await _taskRep.GetAllAsync()).Where(t => t.PerformerId == userId && t.Name.Length < 45).Select(t => Mapper.Map<TaskDto>(t));
        }

        private readonly int _currentYear = DateTime.Now.Year;

        public async Task<IEnumerable<TaskInfo>> TasksFinishedThisYearAsync(int userId)
        {
            return (await _taskRep.GetAllAsync())
                .Where(t => t.PerformerId == userId && t.FinishedAt.Year == _currentYear)
                .Select(t => new TaskInfo {Id = t.Id, Name = t.Name});
        }

        public async Task<IEnumerable<TeamInfo>> SortedTeamsWithAllMembersOlderThan10Async()
        {
            bool IsOlderThan10(int birthYear) => _currentYear - birthYear >= 11;

            var teams = (await _teamRep.GetAllAsync()).ToList();
            var users = (await _userRep.GetAllAsync()).ToList();

            return await System.Threading.Tasks.Task.Run(() => teams.GroupJoin(
                    users,
                    t => t.Id,
                    u => u.TeamId,
                    (team, teamUsers) =>
                    {
                        team.Members = teamUsers.ToList();
                        return team;
                    }).Where(t => t.Members.All(m => IsOlderThan10(m.Birthday.Year)))
                .Select(t => new TeamInfo
                {
                    Id = t.Id,
                    Name = t.Name,
                    Members = Mapper.Map<IEnumerable<UserDto>>(t.Members.OrderByDescending(m => m.RegisteredAt).AsEnumerable())
                }));
        }

        public async Task<IEnumerable<UserAndTasks>> SortedUsersAndTasksAsync()
        {
            var tasks = (await _taskRep.GetAllAsync()).ToList();
            var users = (await _userRep.GetAllAsync()).ToList();

            return await System.Threading.Tasks.Task.Run(() => users.OrderBy(u => u.FirstName)
                .Select(
                    u =>
                    {
                        return new UserAndTasks
                        {
                            User = Mapper.Map<UserDto>(u),
                            Tasks = Mapper.Map<IEnumerable<TaskDto>>(tasks.Where(t => t.PerformerId == u.Id)
                                .OrderByDescending(t => t.Name.Length)
                                .AsEnumerable())
                        };
                    }
                ));
        }

        public async Task<UserReport> UserReportAsync(int userId)
        {
            var projects = (await _projectRep.GetAllAsync()).ToList();
            var tasks = (await _taskRep.GetAllAsync()).ToList();
            var taskStates = (await _taskStateRep.GetAllAsync()).ToList();

            var lastProject = projects.Where(p => p.AuthorId == userId)
                .OrderBy(p => p.CreatedAt)
                .GroupJoin(
                    tasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (project, projectTasks) =>
                    {
                        project.Tasks = projectTasks.ToList();
                        return project;
                    })
                .LastOrDefault();

            var userTasks = tasks.Where(t => t.PerformerId == userId);

            return new UserReport
            {
                User = Mapper.Map<UserDto>(await _userRep.GetByIdAsync(userId)),
                LastProject =  Mapper.Map<ProjectDto>(lastProject),
                UncompletedTaskCount = userTasks.Join(
                    taskStates,
                    t => t.TaskStateId,
                    t => t.Id,
                    (task, taskState) =>
                    {
                        task.TaskState = taskState;
                        return task;
                    }).Count(t => t.TaskState.Value != "Finished"),
                LastProjectTaskCount = lastProject?.Tasks?.Count ?? 0,
                LongestDateTask = Mapper.Map<TaskDto>(userTasks.OrderByDescending(t => t.FinishedAt - t.CreatedAt).FirstOrDefault())
            };
        }

        public async Task<IEnumerable<ProjectReport>> ProjectReportsAsync()
        {
            var projects = (await _projectRep.GetAllAsync()).ToList();
            var tasks = (await _taskRep.GetAllAsync()).ToList();
            var users = (await _userRep.GetAllAsync()).ToList();
            var teams = (await _teamRep.GetAllAsync()).ToList();

            return projects.GroupJoin(
                    tasks,
                    p => p.Id,
                    t => t.ProjectId,
                    (project, projectTasks) =>
                    {
                        project.Tasks = projectTasks.ToList();
                        return project;
                    })
                .Where(p => p.Description.Length > 20 || p.Tasks.Count < 3)
                .Join(
                    teams.GroupJoin(
                        users,
                        t => t.Id,
                        u => u.TeamId,
                        (team, teamUsers) =>
                        {
                            team.Members = teamUsers.ToList();
                            return team;
                        }),
                    p => p.TeamId,
                    t => t.Id,
                    (project, team) =>
                    {
                        project.Team = team;
                        return project;
                    })
                .Select(p =>
                    new ProjectReport
                    {
                        Project = Mapper.Map<ProjectDto>(p),
                        LongestDescriptionTask = Mapper.Map<TaskDto>(p.Tasks.OrderBy(t => t.Description.Length).LastOrDefault()),
                        ShortestNameTask = Mapper.Map<TaskDto>(p.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault()),
                        UserCount = p.Team.Members.Count
                    });
        }
    }
}