using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.BLL.Services.Abstract;
using Structure.Common.DTO.User;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace Structure.BLL.Services
{
    public class UserService : ServiceBase, IUserService
    {
        private readonly IRepository<User> _repository;
        
        public UserService(IRepository<User> repository, IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<UserDto>> GetUsersAsync()
        {
            return (await _repository.GetAllAsync()).Select(u => Mapper.Map<UserDto>(u));
        }

        public async Task<UserDto> GetUserAsync(int id)
        {
            return Mapper.Map<UserDto>(await _repository.GetByIdAsync(id));
        }

        public async Task AddUserAsync(NewUserDto userDto)
        {
            var entity = Mapper.Map<User>(userDto);
            await _repository.AddAsync(entity);
        }

        public async Task RemoveUserAsync(int id)
        {
            await _repository.RemoveAsync(id);
        }

        public async Task UpdateUserAsync(UserDto userDto)
        {
            await _repository.UpdateAsync( Mapper.Map<User>(userDto));
        }
        
        public async Task AddUserToTeamAsync(int userId, int teamId,[FromServices] ITeamService service)
        {
            var user = await _repository.GetByIdAsync(userId);
            if (user != null)
            {
                var team = await service.GetTeamAsync(teamId);
                if (team != null)
                {
                    user.TeamId = team.Id;
                    await _repository.UpdateAsync(user);
                }
            }
        }
    }
}