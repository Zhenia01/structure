using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Structure.BLL.Interfaces;
using Structure.BLL.Services.Abstract;
using Structure.Common.DTO.TaskState;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace Structure.BLL.Services
{
    public class TaskStateService : ServiceBase, ITaskStateService
    {
        private readonly IRepository<TaskState> _repository;
        
        public TaskStateService(IRepository<TaskState> repository, IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<TaskStateDto>> GetTaskStatesAsync()
        {
            return (await _repository.GetAllAsync()).Select(u => Mapper.Map<TaskStateDto>(u));
        }

        public async Task<TaskStateDto> GetTaskStateAsync(int id)
        {
            return Mapper.Map<TaskStateDto>(await _repository.GetByIdAsync(id));
        }

        public async Task AddTaskStateAsync(TaskStateDto taskStateDto)
        {
            var entity = Mapper.Map<TaskState>(taskStateDto);
            await _repository.AddAsync(entity);
        }

        public async Task RemoveTaskStateAsync(int id)
        {
            await _repository.RemoveAsync(id);
        }

        public async Task UpdateTaskStateAsync(TaskStateDto taskStateDto)
        {
            await _repository.UpdateAsync( Mapper.Map<TaskState>(taskStateDto));
        }

        public async Task<TaskStateDto> GetFinishedTaskStateAsync()
        {
            return Mapper.Map<TaskStateDto>((await _repository.GetAllAsync()).First(t => t.Value == "Finished"));
        }
    }
}