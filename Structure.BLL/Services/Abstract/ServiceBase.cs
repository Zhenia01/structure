using AutoMapper;

namespace Structure.BLL.Services.Abstract
{
    public class ServiceBase
    {
        private protected readonly IMapper Mapper;

        public ServiceBase(IMapper mapper)
        {
            Mapper = mapper;
        }
    }
}