using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Structure.BLL.Interfaces;
using Structure.BLL.Services.Abstract;
using Structure.Common.DTO.Project;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace Structure.BLL.Services
{
    public class ProjectService : ServiceBase, IProjectService
    {
        private readonly IRepository<Project> _repository;
        
        public ProjectService(IRepository<Project> repository, IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<ProjectDto>> GetProjectsAsync()
        {
            return (await _repository.GetAllAsync()).Select(u => Mapper.Map<ProjectDto>(u));
        }

        public async Task<ProjectDto> GetProjectAsync(int id)
        {
            return Mapper.Map<ProjectDto>(await _repository.GetByIdAsync(id));
        }

        public async Task AddProjectAsync(NewProjectDto projectDto)
        {
            var entity = Mapper.Map<Project>(projectDto);
            await _repository.AddAsync(entity);
        }

        public async Task RemoveProjectAsync(int id)
        {
            await _repository.RemoveAsync(id);
        }

        public async Task UpdateProjectAsync(ProjectDto projectDto)
        {
            await _repository.UpdateAsync( Mapper.Map<Project>(projectDto));
        }
    }
}