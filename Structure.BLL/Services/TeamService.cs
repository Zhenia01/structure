using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Structure.BLL.Interfaces;
using Structure.BLL.Services.Abstract;
using Structure.Common.DTO.Team;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Task = System.Threading.Tasks.Task;

namespace Structure.BLL.Services
{
    public class TeamService : ServiceBase, ITeamService
    {
        private readonly IRepository<Team> _repository;
        
        public TeamService(IRepository<Team> repository, IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<TeamDto>> GetTeamsAsync()
        {
            return (await _repository.GetAllAsync()).Select(u => Mapper.Map<TeamDto>(u));
        }

        public async Task<TeamDto> GetTeamAsync(int id)
        {
            return Mapper.Map<TeamDto>(await _repository.GetByIdAsync(id));
        }

        public async Task AddTeamAsync(TeamDto teamDto)
        {
            var entity = Mapper.Map<Team>(teamDto);
            await _repository.AddAsync(entity);
        }

        public async Task RemoveTeamAsync(int id)
        {
            await _repository.RemoveAsync(id);
        }

        public async Task UpdateTeamAsync(TeamDto teamDto)
        {
            await _repository.UpdateAsync( Mapper.Map<Team>(teamDto));
        }
    }
}