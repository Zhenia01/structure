using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Structure.BLL.Interfaces;
using Structure.BLL.Services.Abstract;
using Structure.Common.DTO.Task;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;

using Task = System.Threading.Tasks.Task;
using TaskEntity = Structure.DAL.Entities.Task;

namespace Structure.BLL.Services
{
    public class TaskService : ServiceBase, ITaskService
    {
        private readonly IRepository<TaskEntity> _repository;
        
        public TaskService(IRepository<TaskEntity> repository, IMapper mapper) : base(mapper)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<TaskDto>> GetTasksAsync()
        {
            return (await _repository.GetAllAsync()).Select(u => Mapper.Map<TaskDto>(u));
        }

        public async Task<TaskDto> GetTaskAsync(int id)
        {
            return Mapper.Map<TaskDto>(await _repository.GetByIdAsync(id));
        }

        public async Task AddTaskAsync(NewTaskDto taskDto)
        {
            var entity = Mapper.Map<TaskEntity>(taskDto);
            await _repository.AddAsync(entity);
        }

        public async Task RemoveTaskAsync(int id)
        {
            await _repository.RemoveAsync(id);
        }

        public async Task UpdateTaskAsync(TaskDto taskDto)
        {
            await _repository.UpdateAsync( Mapper.Map<TaskEntity>(taskDto));
        }

        public async Task MarkTaskAsFinishedAsync(int id,[FromServices] ITaskStateService service)
        {
            var task = await _repository.GetByIdAsync(id);
            if (task != null)
            {
                task.TaskState = Mapper.Map<TaskState>(await service.GetFinishedTaskStateAsync());
                await _repository.UpdateAsync(task);
            }
        }
    }
}