﻿using System;
using Structure.I.Services;
using Task = System.Threading.Tasks.Task;

namespace Structure.I
{
    class Program
    {
        static async Task Main(string[] args)
        {
            Queries queries = new Queries();
            
            var t1 = queries.TasksPerProjectCountAsync(1);
            var t2 = queries.TasksWithNamesUpTo45Async(1);
            var t3 = queries.TasksFinishedThisYearAsync(1);
            var t4 = queries.SortedTeamsWithAllMembersOlderThan10Async();
            var t5 = queries.SortedUsersAndTasksAsync();
            var t6 = queries.UserReportAsync(1);
            var t7 = queries.ProjectReportsAsync();
            await Task.WhenAll(t1, t2, t3, t4, t5, t6, t7);
            
            Console.WriteLine(
                "1. Отримати кількість тасків у проекті конкретного користувача (по id) (словник, де key буде проект, а value кількість тасків):");
            Console.WriteLine("    Для користувача з id = 1:");
            foreach (var projectTasks in await queries.TasksPerProjectCountAsync(1))
            {
                Console.WriteLine(
                    $"\t Проект {projectTasks.Project.Id} {projectTasks.Project.Name} : {projectTasks.Count}");
            }

            Console.OutputEncoding = System.Text.Encoding.UTF8;

            Console.WriteLine(
                "\n\n\n2. Отримати список тасків, призначених для конкретного користувача (по id), де name таска <45 символів (колекція з тасків)");
            Console.WriteLine("    Для користувача з id = 1:");
            Console.WriteLine("\t Таски:");
            foreach (var task in await t2)
            {
                Console.WriteLine($"\t {task.Id} {task.Name}");
            }

            Console.WriteLine(
                "\n\n\n3. Отримати список (id, name) з колекції тасків, які виконані (finished) в поточному (2020) році для конкретного користувача (по id).");
            Console.WriteLine("    Для користувача з id = 1:");
            Console.WriteLine("\t Таски:");
            foreach (var task in await t3)
            {
                Console.WriteLine($"\t {task.Id} {task.Name}");
            }

            Console.WriteLine(
                "\n\n\n4. Отримати список (id, ім'я команди і список користувачів) з колекції команд, учасники яких старші 10 років, відсортованих за датою реєстрації користувача за спаданням, а також згрупованих по командах.");
            foreach (var teamInfo in await t4)
            {
                Console.WriteLine($"\t Команда {teamInfo.Id} {teamInfo.Name}:");
                foreach (var member in teamInfo.Members)
                {
                    Console.WriteLine($"\t\t {member.Id} {member.FirstName} {member.LastName}");
                }
            }

            Console.WriteLine(
                "\n\n\n5. Отримати список користувачів за алфавітом first_name (по зростанню) з відсортованими tasks по довжині name (за спаданням).");
            foreach (var usersAndTask in await t5)
            {
                Console.WriteLine(
                    $"\t Користувач: {usersAndTask.User.Id} {usersAndTask.User.FirstName} {usersAndTask.User.LastName}:");
                foreach (var task in usersAndTask.Tasks)
                {
                    Console.WriteLine($"\t\t {task.Id} {task.Name}");
                }
            }

            Console.WriteLine("\n\n\n6. Отримати наступну структуру (передати Id користувача в параметри)....");
            Console.WriteLine("    Для користувача з id = 1:");
            var userReport = await t6;
            if (userReport != null)
            {
                Console.WriteLine(
                    $"\t Користувач: {userReport.User.Id} {userReport.User.FirstName} {userReport.User.LastName}");
                Console.WriteLine(
                    $"\t Останній проект користувача (за датою створення): {userReport.LastProject?.Id} {userReport.LastProject?.Name ?? "Немає"}");
                Console.WriteLine(
                    $"\t Загальна кількість тасків під останнім проектом: {userReport.LastProjectTaskCount}");
                Console.WriteLine(
                    $"\t Загальна кількість незавершених або скасованих тасків для користувача: {userReport.UncompletedTaskCount}");
                Console.WriteLine(
                    $"\t Найтриваліший таск користувача за датою (найраніше створений - найпізніше закінчений): {userReport.LongestDateTask.Id} {userReport.LongestDateTask?.Name ?? "Немає"}");
            }
            else
            {
                Console.WriteLine("Немає");
            }

            Console.WriteLine("\n\n\n7. Отримати таку структуру: ......");
            var projectReports = await t7;
            foreach (var projectReport in projectReports)
            {
                Console.WriteLine($"\t Проект: {projectReport.Project.Id}, {projectReport.Project?.Name ?? "Немає"}");
                Console.WriteLine(
                    $"\t Найдовший таск проекту (за описом): {projectReport.LongestDescriptionTask?.Id}, {projectReport.LongestDescriptionTask?.Name ?? "Немає"}");
                Console.WriteLine(
                    $"\t Найкоротший таск проекту (по імені): {projectReport.ShortestNameTask?.Id}, {projectReport.ShortestNameTask?.Name ?? "Немає"}");
                Console.WriteLine(
                    $"\t Загальна кількість користувачів в команді проекту, де або опис проекту >20 символів, або кількість тасків <3: {projectReport.UserCount}");
                Console.WriteLine();
            }

            Console.WriteLine("Press any key to mark random task as finished with 5 sec delay");
            Console.ReadKey();

            try
            {
                Console.WriteLine(await queries.MarkRandomTaskWithDelay(5000));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}