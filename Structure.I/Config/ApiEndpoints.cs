namespace Structure.I.Config
{
    public static class ApiEndpoints
    {
        private const string BaseUrl = "https://localhost:44350/api";

        public static class Linq
        {
            private static readonly string BaseLinqUrl = $"{BaseUrl}/linq/";

            public static string TasksWithNamesUpTo45(int id) => $"{BaseLinqUrl}/TasksWithNamesUpTo45/{id}";

            public static string UserReport(int id) => $"{BaseLinqUrl}/UserReport/{id}";

            public static readonly string ProjectReports = $"{BaseLinqUrl}/ProjectReports";

            public static string TasksFinishedThisYear(int id) => $"{BaseLinqUrl}/TasksFinishedThisYear/{id}";

            public static readonly string SortedTeamsWithAllMembersOlderThan10 =
                $"{BaseLinqUrl}/SortedTeamsWithAllMembersOlderThan10";

            public static readonly string SortedUsersAndTasks = $"{BaseLinqUrl}/SortedUsersAndTasks";

            public static string TasksPerProjectCount(int id) => $"{BaseLinqUrl}/TasksPerProjectCount/{id}";
        }

        public static readonly string GetAllTasks = $"{BaseUrl}/tasks";
        public static string MarkTask(int id) => $"{BaseUrl}/tasks/mark/{id}";
    }
}