using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;
using System.Timers;
using Structure.Common.DTO.Task;
using Structure.Common.LinqQueriesModels;
using Structure.I.Config;
using Task = System.Threading.Tasks.Task;

namespace Structure.I.Services
{
    public class Queries
    {
        private static readonly HttpClient HttpClient;

        static Queries()
        {
            HttpClient = new HttpClient();
        }

        public async Task<IEnumerable<TaskDto>> TasksWithNamesUpTo45Async(int id)
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<TaskDto>>(ApiEndpoints.Linq.TasksWithNamesUpTo45(id))
                    )
                    .ToList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<TaskDto>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<TaskDto>();
            }
        }

        public async Task<UserReport> UserReportAsync(int id)
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<UserReport>(ApiEndpoints.Linq.UserReport(id)));
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return null;
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return null;
            }
        }

        public async Task<IEnumerable<ProjectReport>> ProjectReportsAsync()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<ProjectReport>>(ApiEndpoints.Linq.ProjectReports))
                    .ToList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<ProjectReport>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<ProjectReport>();
            }
        }

        public async Task<IEnumerable<TaskInfo>> TasksFinishedThisYearAsync(int id)
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<TaskInfo>>(
                        ApiEndpoints.Linq.TasksFinishedThisYear(id)))
                    .ToList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<TaskInfo>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<TaskInfo>();
            }
        }

        public async Task<IEnumerable<TeamInfo>> SortedTeamsWithAllMembersOlderThan10Async()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<TeamInfo>>(ApiEndpoints.Linq
                        .SortedTeamsWithAllMembersOlderThan10))
                    .ToList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<TeamInfo>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<TeamInfo>();
            }
        }

        public async Task<IEnumerable<UserAndTasks>> SortedUsersAndTasksAsync()
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<UserAndTasks>>(ApiEndpoints.Linq
                        .SortedUsersAndTasks))
                    .ToList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<UserAndTasks>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<UserAndTasks>();
            }
        }

        public async Task<IEnumerable<ProjectTasks>> TasksPerProjectCountAsync(int id)
        {
            try
            {
                return
                    (await HttpClient.GetFromJsonAsync<IEnumerable<ProjectTasks>>(ApiEndpoints.Linq
                        .TasksPerProjectCount(id)))
                    .ToList();
            }
            catch (HttpRequestException)
            {
                Console.WriteLine("Server error occured");
                return Enumerable.Empty<ProjectTasks>();
            }
            catch (JsonException)
            {
                Console.WriteLine("Invalid body");
                return Enumerable.Empty<ProjectTasks>();
            }
        }

        public async Task<int> MarkRandomTaskWithDelay(int milliseconds = 1000)
        {
            var rng = new Random();
            var timer = new Timer(milliseconds)
            {
                AutoReset = false,
            };
            var tcs = new TaskCompletionSource<int>();

            timer.Elapsed += async (o, e) =>
            {
                try
                {
                    var tasks =
                        (await HttpClient.GetFromJsonAsync<IEnumerable<TaskDto>>(ApiEndpoints.GetAllTasks))
                        .ToList();

                    var id = tasks[rng.Next(0, tasks.Count - 1)].Id;

                    var response = await HttpClient.PatchAsync(ApiEndpoints.MarkTask(id), null);

                    if (response.StatusCode == HttpStatusCode.NotFound) throw new ArgumentException("Wrong id");
                    
                    tcs.SetResult(int.Parse(await response.Content.ReadAsStringAsync()));
                }
                catch (HttpRequestException)
                {
                    Console.WriteLine("Server error occured");
                }
                catch (JsonException)
                {
                    Console.WriteLine("Invalid body");
                }
            };
            timer.Start();
            
            return await tcs.Task;
        }
    }
}