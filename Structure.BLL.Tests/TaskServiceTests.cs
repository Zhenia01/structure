using AutoMapper;
using FakeItEasy;
using Structure.BLL.Interfaces;
using Structure.BLL.Services;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Xunit;

namespace Structure.BLL.Tests
{
    public class TaskServiceTests
    {
        [Fact]
        public async System.Threading.Tasks.Task WhenTaskMarkedAsFinished_ThenRepositoriesUpdateIsHappenedAsync()
        {
            var repository = A.Fake<IRepository<Task>>();
            
            ITaskService service = new TaskService(repository, A.Fake<IMapper>());

            await service.MarkTaskAsFinishedAsync(1, A.Fake<ITaskStateService>());

            A.CallTo(() => repository.UpdateAsync(A<Task>._)).MustHaveHappenedOnceExactly();
        }
    }
}