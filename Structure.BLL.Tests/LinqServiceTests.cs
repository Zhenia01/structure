using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Structure.BLL.Services;
using Structure.Common.DTO.Task;
using Structure.Common.LinqQueriesModels;
using Structure.DAL.Context;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Structure.DAL.Repositories;
using Xunit;
using Task = Structure.DAL.Entities.Task;

namespace Structure.BLL.Tests
{
    public class LinqServiceTests
    {
        private class Repositories
        {
            public IRepository<User> UserRepository { get; }
            public IRepository<Task> TaskRepository { get; }
            public IRepository<Project> ProjectRepository { get; }
            public IRepository<TaskState> TaskStateRepository { get; }
            public IRepository<Team> TeamRepository { get; }

            public Repositories(StorageContext context)
            {
                UserRepository = new UserRepository(context);
                TaskRepository = new TaskRepository(context);
                ProjectRepository = new ProjectRepository(context);
                TaskStateRepository = new TaskStateRepository(context);
                TeamRepository = new TeamRepository(context);
            }
        }

        private LinqService CreateLinqService(Repositories repositories) => new LinqService(
            repositories.ProjectRepository,
            repositories.TeamRepository, repositories.TaskRepository, repositories.TaskStateRepository,
            repositories.UserRepository, null);

        private static DbContextOptions<StorageContext> ContextOptions(MethodBase method) =>
            new DbContextOptionsBuilder<StorageContext>()
                .UseInMemoryDatabase(method!.Name)
                .Options;

        [Fact]
        public async System.Threading.Tasks.Task TasksPerProjectCount_WhenNoProjects_ThenEmptyAsync()
        {
            using var context = new StorageContext(ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var linqService = CreateLinqService(repositories);

            User user = new User();

            await repositories.UserRepository.AddAsync(user);

            Assert.Empty(await linqService.TasksPerProjectCountAsync(user.Id));
        }

        [Fact]
        public async System.Threading.Tasks.Task
            TasksPerProjectCount_When2ProjectsWith1and2Tasks_Then2ProjectsWith1and2TasksDictionaryAsync()
        {
            using var context =
                new StorageContext(ContextOptions(MethodBase.GetCurrentMethod()));
            var repositories = new Repositories(context);

            var user = new User();

            var project1 = new Project {Author = user};

            var p1Task = new Task {Project = project1};

            var project2 = new Project {Author = user};

            var p2Task1 = new Task {Project = project2};
            var p2Task2 = new Task {Project = project2};

            await System.Threading.Tasks.Task.WhenAll(
                repositories.UserRepository.AddAsync(user),
                repositories.ProjectRepository.AddAsync(project1),
                repositories.TaskRepository.AddAsync(p1Task),
                repositories.ProjectRepository.AddAsync(project2),
                repositories.TaskRepository.AddAsync(p2Task1),
                repositories.TaskRepository.AddAsync(p2Task2)
            );

            var linqService = CreateLinqService(repositories);

            var dictResult = await linqService.TasksPerProjectCountAsync(user.Id);

            Assert.Equal(2, dictResult.Keys.Count);
            Assert.Equal(dictResult.Values, new[] {1, 2});
        }

        [Fact]
        public async System.Threading.Tasks.Task TasksWithNamesUpTo45_WhenNoTasks_ThenEmptyAsync()
        {
            using var context = new StorageContext(ContextOptions(MethodBase.GetCurrentMethod()));
            var repositories = new Repositories(context);

            var user = new User();
            await repositories.UserRepository.AddAsync(user);

            var linqService = CreateLinqService(repositories);

            Assert.Empty(await linqService.TasksWithNamesUpTo45Async(user.Id));
        }

        [Fact]
        public async System.Threading.Tasks.Task
            TasksWithNamesUpTo45_When3TasksWithSingleNamedMoreThan45Length_Then2Async()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));
            var repositories = new Repositories(context);

            var user = new User();

            var task1 = new Task {Performer = user, Name = "ShortName1"};
            var task2 = new Task {Performer = user, Name = "ShortName2"};
            var task3 = new Task
            {
                Performer = user,
                Name =
                    "Such a long name                                        OMG                                                            "
            };
            
            var linqService = CreateLinqService(repositories);


            await System.Threading.Tasks.Task.WhenAll(
                repositories.UserRepository.AddAsync(user),
                repositories.TaskRepository.AddAsync(task1),
                repositories.TaskRepository.AddAsync(task2),
                repositories.TaskRepository.AddAsync(task3)).ContinueWith(async t =>
            {
                List<TaskDto> result = (await linqService.TasksWithNamesUpTo45Async(1)).ToList();
                Assert.Equal(result.Select(task => task.Id), new[] {task1.Id, task2.Id});
            });
        }

        [Fact]
        public async System.Threading.Tasks.Task TasksFinishedThisYear_WhenNoUserWithId_ThenEmptyAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var linqService = CreateLinqService(repositories);

            Assert.Empty(await linqService.TasksFinishedThisYearAsync(-100));
        }

        [Fact]
        public async System.Threading.Tasks.Task TasksFinishedThisYear_When3TasksWith1FinishedThisYear_Then1Async()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var user = new User();
            await repositories.UserRepository.AddAsync(user);

            var task1 = new Task {Performer = user, FinishedAt = DateTime.Today};
            var task2 = new Task {Performer = user, FinishedAt = new DateTime(2000, 12, 1)};
            var task3 = new Task {Performer = user, FinishedAt = new DateTime(2010, 12, 1)};
            await System.Threading.Tasks.Task.WhenAll(
                repositories.TaskRepository.AddAsync(task1),
                repositories.TaskRepository.AddAsync(task2),
                repositories.TaskRepository.AddAsync(task3));

            var linqService = CreateLinqService(repositories);

            List<TaskInfo> result = (await linqService.TasksFinishedThisYearAsync(user.Id)).ToList();

            Assert.Single(result);
            Assert.Equal(result.First().Id, task1.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task
            SortedTeamsWithAllMembersOlderThan10_When1TeamWithMemberYoungerThan10_ThenEmptyAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var team = new Team();
            await repositories.TeamRepository.AddAsync(team);

            var user1 = new User {Birthday = new DateTime(2002, 1, 1), Team = team};
            var user2 = new User {Birthday = new DateTime(2008, 1, 12), Team = team};
            var user3 = new User {Birthday = new DateTime(2019, 6, 7), Team = team};
            await repositories.UserRepository.AddAsync(user1);
            await repositories.UserRepository.AddAsync(user2);
            await repositories.UserRepository.AddAsync(user3);

            var linqService = CreateLinqService(repositories);

            Assert.Empty(await linqService.SortedTeamsWithAllMembersOlderThan10Async());
        }

        [Fact]
        public async System.Threading.Tasks.Task
            SortedTeamsWithAllMembersOlderThan10_When3TeamsWith2TeamsWithNoMemberYoungerThan10_ThenSorted2Async()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var team1 = new Team();

            var team1User1 = new User {Birthday = new DateTime(2002, 1, 1), Team = team1, RegisteredAt = DateTime.Now};
            var team1User2 = new User
                {Birthday = new DateTime(2012, 1, 12), Team = team1, RegisteredAt = DateTime.Now.AddYears(-10)};
            var team1User3 = new User
                {Birthday = new DateTime(2001, 6, 7), Team = team1, RegisteredAt = DateTime.Now.AddYears(-5)};

            var team2 = new Team();

            var team2User1 = new User {Birthday = new DateTime(2002, 1, 1), Team = team2, RegisteredAt = DateTime.Now};
            var team2User2 = new User
                {Birthday = new DateTime(2008, 1, 12), Team = team2, RegisteredAt = DateTime.Now.AddYears(-10)};
            var team2User3 = new User
                {Birthday = new DateTime(2001, 6, 7), Team = team2, RegisteredAt = DateTime.Now.AddYears(-5)};

            var team3 = new Team();


            var team3User1 = new User
                {Birthday = new DateTime(2002, 1, 1), Team = team3, RegisteredAt = DateTime.Now.AddYears(-3)};
            var team3User2 = new User
                {Birthday = new DateTime(2008, 1, 12), Team = team3, RegisteredAt = DateTime.Now.AddYears(-7)};
            var team3User3 = new User
                {Birthday = new DateTime(2001, 6, 7), Team = team3, RegisteredAt = DateTime.Now.AddYears(-5)};

            var linqService = CreateLinqService(repositories);
            
            await System.Threading.Tasks.Task.WhenAll(
                repositories.UserRepository.AddAsync(team3User1),
                repositories.UserRepository.AddAsync(team3User2),
                repositories.UserRepository.AddAsync(team3User3),
                repositories.TeamRepository.AddAsync(team1),
                repositories.UserRepository.AddAsync(team1User1),
                repositories.UserRepository.AddAsync(team1User2),
                repositories.UserRepository.AddAsync(team1User3),
                repositories.TeamRepository.AddAsync(team2),
                repositories.UserRepository.AddAsync(team2User1),
                repositories.UserRepository.AddAsync(team2User2),
                repositories.UserRepository.AddAsync(team2User3),
                repositories.TeamRepository.AddAsync(team3)).ContinueWith(async t =>
            {
                List<TeamInfo> result = (await linqService.SortedTeamsWithAllMembersOlderThan10Async()).ToList();

                Assert.Equal(2, result.Count);
                Assert.Equal(result[0].Members.OrderByDescending(m => m.RegisteredAt), result[0].Members);
                Assert.Equal(result[1].Members.OrderByDescending(m => m.RegisteredAt), result[1].Members);
            });
        }

        [Fact]
        public async System.Threading.Tasks.Task SortedUsersAndTasks_WhenNoUsers_ThenEmptyAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var linqService = CreateLinqService(repositories);

            await linqService.SortedUsersAndTasksAsync().ContinueWith(async t =>
            {
                Assert.Empty(await t);
            });
        }

        [Fact]
        public async System.Threading.Tasks.Task
            SortedUsersAndTasks_WhenUsersAndTasksArePresent_ThenSortedUsersAndTasksAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var user1 = new User {FirstName = "Oleksii"};
            var user2 = new User {FirstName = "Volodymyr"};

            var user1Task1 = new Task {Performer = user1, Name = "Lorem"};
            var user1Task2 = new Task {Performer = user1, Name = "Ipsum"};
            var user2Task1 = new Task {Performer = user2, Name = "RandomName"};
            var user2Task2 = new Task {Performer = user2, Name = "Lor"};
            var user2Task3 = new Task {Performer = user2, Name = "Em"};
            
            var linqService = CreateLinqService(repositories);

            await System.Threading.Tasks.Task.WhenAll(
                repositories.UserRepository.AddAsync(user1),
                repositories.UserRepository.AddAsync(user2),
                repositories.TaskRepository.AddAsync(user1Task1),
                repositories.TaskRepository.AddAsync(user1Task2),
                repositories.TaskRepository.AddAsync(user2Task1),
                repositories.TaskRepository.AddAsync(user2Task2),
                repositories.TaskRepository.AddAsync(user2Task3)).ContinueWith(async t =>
            {
                List<UserAndTasks> result = (await linqService.SortedUsersAndTasksAsync()).ToList();

                Assert.Equal(2, result.Count);
                Assert.Equal(result.Select(ut => ut.User).OrderBy(u => u.FirstName), result.Select(ut => ut.User));
                foreach (UserAndTasks ut in result)
                {
                    Assert.Equal(ut.Tasks.OrderByDescending(task => task.Name.Length), ut.Tasks);
                }
            });
        }

        [Fact]
        public async System.Threading.Tasks.Task UserReport_WhenNoProjects_ThenEmptyReportAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var user = new User();
            await repositories.UserRepository.AddAsync(user);

            var linqService = CreateLinqService(repositories);

            var result = await linqService.UserReportAsync(user.Id);

            Assert.Equal(user.Id, result.User.Id);
            Assert.Null(result.LastProject);
            Assert.Equal(0, result.LastProjectTaskCount);
            Assert.Equal(0, result.UncompletedTaskCount);
            Assert.Null(result.LongestDateTask);
        }

        [Fact]
        public async System.Threading.Tasks.Task UserReport_WhenDataIsPresent_ThenFullReportAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var user = new User();
            await repositories.UserRepository.AddAsync(user);

            var project1 = new Project {Author = user, CreatedAt = DateTime.Now};
            var project2 = new Project {Author = user, CreatedAt = DateTime.Now.AddYears(-2)};
            await repositories.ProjectRepository.AddAsync(project1);
            await repositories.ProjectRepository.AddAsync(project2);

            var finishedState = new TaskState {Value = "Finished"};
            var startedState = new TaskState {Value = "Started"};
            var createdState = new TaskState {Value = "Created"};
            var canceledState = new TaskState {Value = "Canceled"};
            await repositories.TaskStateRepository.AddAsync(finishedState);
            await repositories.TaskStateRepository.AddAsync(canceledState);
            await repositories.TaskStateRepository.AddAsync(createdState);
            await repositories.TaskStateRepository.AddAsync(startedState);

            var task1 = new Task
            {
                Project = project1, Performer = user, TaskState = finishedState, CreatedAt = DateTime.Now.AddYears(-2),
                FinishedAt = DateTime.Now
            };
            var task2 = new Task
            {
                Project = project1, Performer = user, TaskState = finishedState, CreatedAt = DateTime.Now.AddYears(-3),
                FinishedAt = DateTime.Now
            };
            var task3 = new Task
            {
                Project = project1, Performer = user, TaskState = createdState, CreatedAt = DateTime.Now.AddYears(-4),
                FinishedAt = DateTime.Now
            };
            var task4 = new Task
            {
                Project = project2, Performer = user, TaskState = canceledState, CreatedAt = DateTime.Now.AddYears(-1),
                FinishedAt = DateTime.Now
            };
            var task5 = new Task
            {
                Project = project2, Performer = user, TaskState = startedState, CreatedAt = DateTime.Now.AddYears(-1),
                FinishedAt = DateTime.Now
            };
            await repositories.TaskRepository.AddAsync(task1);
            await repositories.TaskRepository.AddAsync(task2);
            await repositories.TaskRepository.AddAsync(task3);
            await repositories.TaskRepository.AddAsync(task4);
            await repositories.TaskRepository.AddAsync(task5);

            var linqService = CreateLinqService(repositories);

            UserReport result = await linqService.UserReportAsync(user.Id);

            Assert.Equal(user.Id, result.User.Id);
            Assert.Equal(project1.Id, result.LastProject.Id);
            Assert.Equal(3, result.LastProjectTaskCount);
            Assert.Equal(3, result.UncompletedTaskCount);
            Assert.Equal(task3.Id, result.LongestDateTask.Id);
        }

        [Fact]
        public async System.Threading.Tasks.Task ProjectReports_WhenNoProjects_ThenEmptyAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var linqService = CreateLinqService(repositories);

            Assert.Empty(await linqService.ProjectReportsAsync());
        }

        [Fact]
        public async System.Threading.Tasks.Task ProjectReports_When2ProjectAndNoTasks_ThenEmptyAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var project1 = new Project {Description = "LongDescription........................................."};
            var project2 = new Project {Description = "456789"};
            
            
            var linqService = CreateLinqService(repositories);
            
            await System.Threading.Tasks.Task.WhenAll(
                repositories.ProjectRepository.AddAsync(project1),
                repositories.ProjectRepository.AddAsync(project2)).ContinueWith(async t =>
            {
                Assert.Empty(await linqService.ProjectReportsAsync());
            });

        }

        [Fact]
        public async System.Threading.Tasks.Task ProjectReports_When2ProjectsWith1Incorrect_Then1FullReportAsync()
        {
            using var context =
                new StorageContext(
                    ContextOptions(MethodBase.GetCurrentMethod()));

            var repositories = new Repositories(context);

            var project1 = new Project {Description = "LongDescription........................................."};

            var project1Task1 = new Task {Project = project1, Name = "ShortName", Description = "LongDescription"};
            var project1Task2 = new Task {Project = project1, Name = "LongLongName", Description = "shortDescr"};

            var team = new Team();
            
            var user1 = new User {FirstName = "Oleksii", Team = team};
            var user2 = new User {FirstName = "Volodymyr", Team = team};
            
            await System.Threading.Tasks.Task.WhenAll(
                repositories.ProjectRepository.AddAsync(project1),
                repositories.TaskRepository.AddAsync(project1Task1),
                repositories.TaskRepository.AddAsync(project1Task2),
                repositories.TeamRepository.AddAsync(team),
                repositories.UserRepository.AddAsync(user1),
                repositories.UserRepository.AddAsync(user2));

            project1.Team = team;
            await repositories.ProjectRepository.UpdateAsync(project1);

            var project2 = new Project {Description = "TooShort"};
            await repositories.ProjectRepository.AddAsync(project2);

            var project2Task1 = new Task {Project = project2, Name = "ShortName", Description = "LongDescription"};
            var project2Task2 = new Task {Project = project2, Name = "LongLongName", Description = "shortDescr"};
            var project2Task3 = new Task {Project = project2, Name = "LongLongName", Description = "shortDescr"};
            var project2Task4 = new Task {Project = project2, Name = "LongLongName", Description = "shortDescr"};
            
            
            var linqService = CreateLinqService(repositories);
            
            await System.Threading.Tasks.Task.WhenAll(
                repositories.TaskRepository.AddAsync(project2Task1),
                repositories.TaskRepository.AddAsync(project2Task2),
                repositories.TaskRepository.AddAsync(project2Task3),
                repositories.TaskRepository.AddAsync(project2Task4)).ContinueWith(async t =>
            {
                List<ProjectReport> result = (await linqService.ProjectReportsAsync()).ToList();

                Assert.Single(result);
                var report = result.First();
                Assert.Equal(report.Project.Id, project1.Id);
                Assert.Equal(report.LongestDescriptionTask.Id, project1Task1.Id);
                Assert.Equal(report.ShortestNameTask.Id, project1Task1.Id);
                Assert.Equal(2, report.UserCount);
            });
        }
    }
}