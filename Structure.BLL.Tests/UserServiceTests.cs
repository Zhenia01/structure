using AutoMapper;
using FakeItEasy;
using Structure.BLL.Interfaces;
using Structure.BLL.Services;
using Structure.Common.DTO.User;
using Structure.DAL.Entities;
using Structure.DAL.Interfaces;
using Xunit;
using Task = System.Threading.Tasks.Task;

namespace Structure.BLL.Tests
{
    public class UserServiceTests
    {

        [Fact]
        public async Task WhenUserAdded_ThenRepositoriesAddIsHappened()
        {
            var repository = A.Fake<IRepository<User>>();
            
            IUserService service = new UserService(repository, A.Fake<IMapper>());

            await service.AddUserAsync(new NewUserDto());

            A.CallTo(() => repository.AddAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }
        
        [Fact]
        public async Task WhenUserAddedToTeam_ThenRepositoriesUpdateIsHappened()
        {
            var repository = A.Fake<IRepository<User>>();
            
            IUserService service = new UserService(repository, A.Fake<IMapper>());

            await service.AddUserToTeamAsync(1, 2,A.Fake<ITeamService>());

            A.CallTo(() => repository.UpdateAsync(A<User>._)).MustHaveHappenedOnceExactly();
        }
    }
}