using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using Microsoft.AspNetCore.Mvc.Testing;
using Structure.Common.DTO.Project;
using Structure.Common.DTO.Team;
using Structure.Common.DTO.User;
using Xunit;

namespace Structure.WebAPI.IntegrationTests
{
    public class ProjectsControllerIntegrationTests : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        private readonly HttpClient _client;
        
        public ProjectsControllerIntegrationTests(CustomWebApplicationFactory<Startup> factory)
        {
            _client = factory.CreateClient(new WebApplicationFactoryClientOptions
            {
                AllowAutoRedirect = false
            });
        }

        [Fact]
        public async System.Threading.Tasks.Task CreateProject_ThenResponseCreatedAndCorrespondingBodyAsync()
        {
            var newUser = new NewUserDto
            {
                Birthday = DateTime.Now,
                Email = "2dsds@gmail.com",
                FirstName = "Zhenia",
                LastName = "Borodaikevych",
                RegisteredAt = DateTime.Now.AddYears(-5)
            };

            var createdUser = await (await _client.PostAsJsonAsync("api/users", newUser)).Content.ReadFromJsonAsync<UserDto>();
            
            var team = new TeamDto
            {
                CreatedAt = DateTime.Now,
                Name = "Lorem"
            };

            int teamId = (await (await _client.PostAsJsonAsync("api/teams", team)).Content.ReadFromJsonAsync<TeamDto>()).Id;

            var newProject = new NewProjectDto
            {
                AuthorId = createdUser.Id,
                CreatedAt = DateTime.Now,
                Deadline = DateTime.Now.AddYears(1),
                Description = "Lorem",
                Name = "Ipsum",
                TeamId = teamId
            };

            var httpResponse = await _client.PostAsJsonAsync("api/projects", newProject);

            ProjectDto createdProject = await httpResponse.Content.ReadFromJsonAsync<ProjectDto>();

            Assert.Equal(HttpStatusCode.Created, httpResponse.StatusCode);
            Assert.Equal(newProject.AuthorId, createdProject.AuthorId);
            Assert.Equal(newProject.TeamId, createdProject.TeamId);
        }
        
        [Fact]
        public async System.Threading.Tasks.Task CreateNullProject_ThenBadRequestResponseAsync()
        {
            var httpResponse = await _client.PostAsJsonAsync<NewProjectDto>("api/projects", null);

            Assert.Equal(HttpStatusCode.BadRequest, httpResponse.StatusCode);
        }
    }
}