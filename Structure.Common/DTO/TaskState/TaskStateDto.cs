namespace Structure.Common.DTO.TaskState
{
    public class TaskStateDto
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}