using System;

namespace Structure.Common.DTO.Task
{
    public class NewTaskDto
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public int State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
    }
}