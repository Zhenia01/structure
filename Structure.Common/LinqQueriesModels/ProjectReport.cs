using Structure.Common.DTO.Project;
using Structure.Common.DTO.Task;

namespace Structure.Common.LinqQueriesModels
{
    public class ProjectReport
    {
        public ProjectDto Project { get; set; }
        public TaskDto LongestDescriptionTask { get; set; }
        public TaskDto ShortestNameTask { get; set; }
        public int UserCount { get; set; }
    }
}