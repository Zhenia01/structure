using System.Collections.Generic;
using Structure.Common.DTO.User;

namespace Structure.Common.LinqQueriesModels
{
    public class TeamInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<UserDto> Members { get; set; }
    }
}