using System.Collections.Generic;
using Structure.Common.DTO.Task;
using Structure.Common.DTO.User;

namespace Structure.Common.LinqQueriesModels
{
    public class UserAndTasks
    {
        public UserDto User { get; set; }
        public IEnumerable<TaskDto> Tasks { get; set; }
    }
}