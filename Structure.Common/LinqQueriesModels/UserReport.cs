using Structure.Common.DTO.Project;
using Structure.Common.DTO.Task;
using Structure.Common.DTO.User;

namespace Structure.Common.LinqQueriesModels
{
    public class UserReport
    {
        public UserDto User { get; set; }
        public ProjectDto LastProject { get; set; }
        public int LastProjectTaskCount { get; set; }
        public int UncompletedTaskCount { get; set; }
        public TaskDto LongestDateTask { get; set; }
    }
}