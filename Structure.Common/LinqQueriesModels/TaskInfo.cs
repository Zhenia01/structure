namespace Structure.Common.LinqQueriesModels
{
    public class TaskInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}