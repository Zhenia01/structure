using Structure.Common.DTO.Project;

namespace Structure.Common.LinqQueriesModels
{
    public class ProjectTasks
    {
        public ProjectDto Project { get; set; }
        public int Count { get; set; }
    }
}